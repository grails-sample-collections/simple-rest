# Simple REST Grails

Install [httpie](https://httpie.org/) first.

Run this project by this command : `grails` then `run-app`

Request using curl:

`curl -i -H "Accept: application/json" localhost:8080/article/1`

or

`http localhost:8080/article/1`

Response :

```
HTTP/1.1 200
Content-Type: application/json;charset=UTF-8
Date: Sun, 09 Dec 2018 09:11:57 GMT
Transfer-Encoding: chunked
X-Application-Context: application:development

{
    "author": "Hendi Santika",
    "category": {
        "id": 1
    },
    "content": "Step by step tutorial on how to create Grails web application from scratch",
    "createDate": "2018-12-09T09:04:00Z",
    "description": "Step by step tutorial on how to create Grails web application from scratch",
    "id": 1,
    "title": "How to Create Grails Web Application"
}
```

Add new article `POST /article`

Request :

`curl -i -X POST -H "Content-Type: application/json" -d '{"category":{"id":1},"title":"Article Title 2","author":"Hendi Santika","description":"Article Description 2","content":"Article Content 2"}' localhost:8080/article`

Response :

```
HTTP/1.1 201
X-Application-Context: application:development
Location: http://localhost:8080/article/6
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Sun, 09 Dec 2018 09:25:19 GMT

{
  "id": 6,
  "title": "Article Title 2",
  "category": {
    "id": 1
  },
  "content": "Article Content 2",
  "description": "Article Description 2",
  "createDate": "2018-12-09T09:25:19Z",
  "author": "Hendi Santika"
}

```

Edit data `PUT /article/2`

Request :

`curl -i -X PUT -H "Content-Type: application/json" -d '{"title":"Programming Cobol"}' localhost:8080/article/2`

Response :

```
HTTP/1.1 200
X-Application-Context: application:development
Location: http://localhost:8080/article/2
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Sun, 09 Dec 2018 09:29:05 GMT

{
  "id": 2,
  "title": "Programming Cobol",
  "category": {
    "id": 1
  },
  "content": "Article Content 2",
  "description": "Article Description 2",
  "createDate": "2018-12-09T09:14:04Z",
  "author": "Hendi Santika"
}
```

Delete a data :

`DELETE /article/3`

Request :

`curl -i -X DELETE localhost:8080/article/3`

Response :

```
HTTP/1.1 204
X-Application-Context: application:development
Date: Sun, 09 Dec 2018 09:31:06 GMT
```


|Method|Endpoints|Notes|
|---|---|---|
|GET 	|/article 	|Get all articles data|
|GET 	|/article/1 |Get single article|
|POST 	|/article 	|Post data|
|PUT 	|/article/1 |Update data|
|DELETE |/article/1 |Delete data|